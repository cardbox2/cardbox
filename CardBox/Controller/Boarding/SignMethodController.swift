//
//  SignMethod.swift
//  CardBox
//
//  Created by Thomas Gallier on 12/04/2021.
//

import Foundation
import UIKit

class SignMethodController:UIViewController {
		
	// MARK: Outlet
	
	@IBOutlet weak var btnSignUp: UIButton!
	@IBOutlet weak var btnSignIn: UIButton!
	
	// MARK: Life Cycle
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.hideKeyboardWhenTappedAround()
	}
	
	// MARK: Action
	
	@IBAction func tapSignUp(_ sender: Any) {
		Vibration.selection.vibrate()
		let signUpNameController = self.storyboard?.instantiateViewController(identifier: ControllerName.signUpName.rawValue) as! SignUpNameController
		self.navigationController?.pushViewController(signUpNameController, animated: true)
	}
	
	@IBAction func tapSignIn(_ sender: Any) {
		Vibration.selection.vibrate()
		let signInMethodController = self.storyboard?.instantiateViewController(identifier: ControllerName.signInMethod.rawValue) as! SignInMethodController
		self.navigationController?.pushViewController(signInMethodController, animated: true)
	}
}
