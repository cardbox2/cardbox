//
//  SignInEmailController.swift
//  CardBox
//
//  Created by Thomas Gallier on 14/04/2021.
//

import Foundation
import UIKit
import FirebaseAuth
import FirebaseFirestore

class SignInEmailController:UIViewController, UITextFieldDelegate, APIFetcherDelegate {
	
	// MARK: Outlet
	
	@IBOutlet weak var viewLoader: UIView!
	@IBOutlet weak var textFieldPassword: UITextField!
	@IBOutlet weak var textFieldEmail: UITextField!
	@IBOutlet weak var btnShowPassword: UIButton!
	@IBOutlet weak var btnConnect: UIButton!
	@IBOutlet weak var btnBack: UIButton!
	
	// MARK: Life Cycle
	
	override func viewDidLoad() {
		super.viewDidLoad()
		navigationController?.interactivePopGestureRecognizer?.delegate = nil
		self.hideKeyboardWhenTappedAround()
		self.textFieldEmail.delegate = self
		self.textFieldPassword.delegate = self
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(true)
		self.viewLoader.isHidden = true
		self.textFieldEmail.becomeFirstResponder()
	}
	
	// MARK: Action
	
	@IBAction func tapBack(_ sender: Any) {
		self.navigationController?.popViewController(animated: true)
	}
	
	@IBAction func tapShowPassword(_ sender: Any) {
		self.btnShowPassword.tintColor = self.textFieldPassword.isSecureTextEntry ? UIColor.label : UIColor.systemGray2
		self.textFieldPassword.isSecureTextEntry = !self.textFieldPassword.isSecureTextEntry
	}
	
	@IBAction func tapConnect(_ sender: Any) {
		Vibration.selection.vibrate()
		if let email = self.textFieldEmail.text, let password = self.textFieldPassword.text {
			self.viewLoader.isHidden = false
			AuthManager.shared.signIn(email: email, password: password) { (error) in
				if error != nil {
					self.viewLoader.isHidden = true
					self.showAlert(title: "Une erreur est survenue.", description: error?.localizedDescription ?? "Veuillez réessayer.")
				}
				else { self.request() }
			}
		}
	}
	
	// MARK: Delegate request
	
	func goMain() {
		let mainStoryboard = UIStoryboard(name: StoryboardName.main.rawValue, bundle: nil).instantiateViewController(identifier: ControllerName.home.rawValue) as! HomeController
		let nav = UIStoryboard(name: StoryboardName.main.rawValue, bundle: nil).instantiateInitialViewController() as! UINavigationController
		nav.viewControllers = [mainStoryboard]
		UIApplication.shared.windows.first?.rootViewController = nav
		UserStorage.newUser = [:]
	}
	
	func request() {
		APIFetcher.shared.request(.cards, delegate: self) { (response) in
			self.responseFetchCard(response: response)
		}
	}
	
	func responseFetchCard(response : Error?) {
		if let _ = response { return }
		else {
			AuthManager.shared.user.cards = APIFetcher.shared.allCached()
			goMain()
		}
	}
	
	func returnCacheRequest(completion: Error?) { self.responseFetchCard(response: completion) }
	
	// MARK: Text field delegate
	
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		textField.resignFirstResponder()
		if textField == self.textFieldEmail { textFieldPassword.becomeFirstResponder() }
		else { tapConnect(textField) }
		return true
	}
	
}
