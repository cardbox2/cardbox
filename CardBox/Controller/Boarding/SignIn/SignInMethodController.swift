//
//  SignInMethodController.swift
//  CardBox
//
//  Created by Thomas Gallier on 14/04/2021.
//

import Foundation
import UIKit

class SignInMethodController:UIViewController {
	
	// MARK: Outlet
	
	@IBOutlet weak var btnEmail: UIButton!
	@IBOutlet weak var btnPhone: UIButton!
	@IBOutlet weak var btnBack: UIButton!
	
	// MARK: Life Cycle
	
	override func viewDidLoad() {
		super.viewDidLoad()
		navigationController?.interactivePopGestureRecognizer?.delegate = nil
		self.hideKeyboardWhenTappedAround()
	}
	
	// MARK: Action
	
	@IBAction func tapBack(_ sender: Any) {
		self.navigationController?.popViewController(animated: true)
	}
	
	@IBAction func tapEmail(_ sender: Any) {
		Vibration.selection.vibrate()
		let signInEmailController = self.storyboard?.instantiateViewController(identifier: ControllerName.signInEmail.rawValue) as! SignInEmailController
		self.navigationController?.pushViewController(signInEmailController, animated: true)
	}	
}
