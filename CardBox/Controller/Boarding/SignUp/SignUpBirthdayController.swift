//
//  SignUpBirthdayController.swift
//  CardBox
//
//  Created by Thomas Gallier on 12/04/2021.
//

import Foundation
import UIKit

class SignUpBirthdayController:UIViewController, UITextFieldDelegate {
		
	// MARK: Outlet
	
	@IBOutlet weak var btnBack: UIButton!
	@IBOutlet weak var btnNext: UIButton!
	@IBOutlet weak var textFieldBirthday: UITextField!
	
	// MARK: Life Cycle
	
	override func viewDidLoad() {
		super.viewDidLoad()
		navigationController?.interactivePopGestureRecognizer?.delegate = nil
		self.textFieldBirthday.delegate = self
		self.hideKeyboardWhenTappedAround()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(true)
		self.textFieldBirthday.becomeFirstResponder()
		if let b = UserStorage.newUser[UserStorage.newUserKeys.birthday.rawValue], b.count > 0 {
			 self.textFieldBirthday.text = b
		}
	}
	
	// MARK: Action
	
	@IBAction func tapBack(_ sender: Any) {
		self.navigationController?.popViewController(animated: true)
	}
	
	@IBAction func tapNext(_ sender: Any) {
		Vibration.selection.vibrate()
		if !(self.textFieldBirthday.text?.isEmpty ?? false) {
			if self.textFieldBirthday.text?.matchesRegex(regex: Regex.birthday) ?? false {
				UserStorage.newUser[UserStorage.newUserKeys.birthday.rawValue] = self.textFieldBirthday.text
				let signUpMethodController = self.storyboard?.instantiateViewController(identifier: ControllerName.signUpMethod.rawValue) as! SignUpMethodController
				self.navigationController?.pushViewController(signUpMethodController, animated: true)
			} else {
				self.showAlert(title: "Mauvais format.", description: "La date doit être au format 'dd/mm/yyyy'.")
			}
		} else {
			self.showAlert(title: "Une erreur est survenue.", description: "La date ne doit pas être vide.")
		}
	}
	
	// MARK: Text field delegate
	
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		textField.resignFirstResponder()
		tapNext(textField)
		return true
	}
	
	
}
