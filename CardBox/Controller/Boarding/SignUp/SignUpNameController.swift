//
//  SignUpNameController.swift
//  CardBox
//
//  Created by Thomas Gallier on 12/04/2021.
//

import Foundation
import UIKit

class SignUpNameController:UIViewController, UITextFieldDelegate {
		
	// MARK: Outlet
	
	@IBOutlet weak var btnBack: UIButton!
	@IBOutlet weak var btnNext: UIButton!
	@IBOutlet weak var textFieldName: UITextField!
	
	// MARK: Life Cycle
	
	override func viewDidLoad() {
		super.viewDidLoad()
		navigationController?.interactivePopGestureRecognizer?.delegate = nil
		self.textFieldName.delegate = self
		self.hideKeyboardWhenTappedAround()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(true)
		self.textFieldName.becomeFirstResponder()
		if let n = UserStorage.newUser[UserStorage.newUserKeys.name.rawValue], n.count > 0 {
			self.textFieldName.text = n
		}
		
	}
	
	// MARK: Action
	
	@IBAction func tapBack(_ sender: Any) {
		self.navigationController?.popViewController(animated: true)
	}
	
	@IBAction func tapNext(_ sender: Any) {
		Vibration.selection.vibrate()
		if self.textFieldName.text?.count ?? 0 > 1 {
			if self.textFieldName.text?.matchesRegex(regex: Regex.name) ?? false{
				UserStorage.newUser[UserStorage.newUserKeys.name.rawValue] = self.textFieldName.text
				let signUpBirthdayController = self.storyboard?.instantiateViewController(identifier: ControllerName.signUpBirthday.rawValue) as! SignUpBirthdayController
				self.navigationController?.pushViewController(signUpBirthdayController, animated: true)
			} else {
				self.showAlert(title: "Une erreur est survenue.", description: "Le prénom est dans un mauvais format")
			}
		} else {
			self.showAlert(title: "Une erreur est survenue.", description: "Le prénom ne doit pas être vide.")
		}
	}	
	
	// MARK: Text field delegate
	
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		textField.resignFirstResponder()
		tapNext(textField)
		return true
	}
	
	
}
