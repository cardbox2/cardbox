//
//  SignUpMethodController.swift
//  CardBox
//
//  Created by Thomas Gallier on 12/04/2021.
//

import Foundation
import UIKit

class SignUpMethodController:UIViewController {
	
	// MARK: Outlet
	
	@IBOutlet weak var btnEmail: UIButton!
	@IBOutlet weak var btnPhone: UIButton!
	@IBOutlet weak var btnBack: UIButton!
	
	// MARK: Life Cycle
	
	override func viewDidLoad() {
		super.viewDidLoad()
		navigationController?.interactivePopGestureRecognizer?.delegate = nil
		self.hideKeyboardWhenTappedAround()
	}
	
	// MARK: Action
	
	@IBAction func tapBack(_ sender: Any) {
		self.navigationController?.popViewController(animated: true)
	}
	
	@IBAction func tapEmail(_ sender: Any) {
		Vibration.selection.vibrate()
		UserStorage.newUser[UserStorage.newUserKeys.sign_up_with.rawValue] = "email"
		let signUpEmailController = self.storyboard?.instantiateViewController(identifier: ControllerName.signUpEmail.rawValue) as! SignUpEmailController
		self.navigationController?.pushViewController(signUpEmailController, animated: true)
	}	
}
