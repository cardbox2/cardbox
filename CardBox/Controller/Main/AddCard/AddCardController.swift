//
//  AddCardController.swift
//  CardBox
//
//  Created by Thomas Gallier on 13/04/2021.
//

import Foundation
import UIKit

class AddCardController:UIViewController {
	
	// MARK: Variable
	
	let maxLengthNumber = 16
	
	// MARK: Outlet
	
	@IBOutlet weak var labelPlaceholder: UILabel!
	@IBOutlet weak var btnReset: UIButton!
	@IBOutlet weak var btnNext: UIButton!
	@IBOutlet weak var labelCardNumber: UILabel!
	@IBOutlet weak var mainStackviewNumber: UIStackView!
	
	// MARK: Life Cycle
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.setupAnimation()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(true)
		self.updateUI()
	}
	
	// MARK: Action
	
	@IBAction func tapClean(_ sender: Any) {
		Vibration.selection.vibrate()
		self.labelCardNumber.text = ""
		self.updateUI()
	}
	
	@IBAction func tapNext(_ sender: Any) {
		Vibration.selection.vibrate()
		UserStorage.newCard[UserStorage.newCardKeys.number.rawValue] = self.labelCardNumber.text
		if self.removeSpaces(text: self.labelCardNumber.text ?? "").count == self.maxLengthNumber {
			let expirationCardController = self.storyboard?.instantiateViewController(identifier: ControllerName.expirationCard.rawValue) as! ExpirationCardController
			expirationCardController.modalPresentationStyle = .fullScreen
			expirationCardController.modalTransitionStyle = .crossDissolve
			self.present(expirationCardController, animated: false, completion: nil)
		}
	}
	
	@IBAction func tapRemoveChar(_ sender: Any) {
		Vibration.selection.vibrate()
		self.labelCardNumber.text = String(self.labelCardNumber.text?.dropLast() ?? "")
		if self.labelCardNumber.text?.last == " " {
			self.tapRemoveChar(sender)
		}
		self.updateUI()
	}
	
	@IBAction func tapBack(_ sender: Any) {
		UserStorage.newCard = [:]
		self.dismiss(animated: true, completion: nil)
	}
	
	// MARK: Function
	
	func setupAnimation() {
		self.mainStackviewNumber.arrangedSubviews.forEach { (s) in
			(s as? UIStackView)?.arrangedSubviews.forEach({ (button) in
				if (button as? UIButton)?.titleLabel?.text != "x" && (button as? UIButton)?.titleLabel?.text != ">" {
					(button as? UIButton)?.addTarget(self, action: #selector(self.clicBtn), for: .touchUpInside)
				}
			})
		}
	}
	
	func removeSpaces(text: String) -> String {
		return String(text).replacingOccurrences(of: " ", with: "")
	}
	
	func addCharactereToCardLabel(char: String) {
		self.labelCardNumber.text = (self.labelCardNumber.text ?? "") + char
	}
	
	func updateUI() {
		UIView.animate(withDuration: 0.4, animations: {
			let clic = self.removeSpaces(text: self.labelCardNumber.text ?? "").count == self.maxLengthNumber
			self.mainStackviewNumber.arrangedSubviews.forEach { (s) in
				(s as? UIStackView)?.arrangedSubviews.forEach({ (button) in
					if let b = button as? UIButton, b.titleLabel?.text != "x", b.titleLabel?.text != ">" {
						b.isUserInteractionEnabled = !clic
						b.setTitleColor(!clic ? UIColor.label : UIColor.secondaryLabel, for: .normal)
					}
				})
			}
			self.labelPlaceholder.isHidden = !(self.labelCardNumber.text?.count == 0)
		})
	}
	
	// MARK: Object function
	
	@objc func clicBtn(button: UIButton) {
		Vibration.selection.vibrate()
		if self.removeSpaces(text: self.labelCardNumber.text ?? "").count % 4 == 0 {
			self.addCharactereToCardLabel(char: " ")
		}
		self.addCharactereToCardLabel(char: (button.titleLabel?.text)!)
		self.updateUI()
		UIView.animate(withDuration: 0.1, delay: 0, options: .allowUserInteraction, animations: {
			button.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
			
		}) { _ in
			UIView.animate(withDuration: 0.1, delay: 0, options: .allowUserInteraction, animations: {
				button.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
			}) { _ in
				UIView.animate(withDuration: 0.1, delay: 0, options: .allowUserInteraction, animations: {
					button.transform = CGAffineTransform.identity
				})
			}
		}
	}
}
