//
//  CVVCardController.swift
//  CardBox
//
//  Created by Thomas Gallier on 13/04/2021.
//

import Foundation
import UIKit

class CVVCardController:UIViewController, APIFetcherDelegate {
	
	// MARK: Variable
	
	let maxLengthCCV = 3
	
	// MARK: Outlet
	
	@IBOutlet weak var labelPlaceholder: UILabel!
	@IBOutlet weak var btnReset: UIButton!
	@IBOutlet weak var btnNext: UIButton!
	@IBOutlet weak var labelCardCVV: UILabel!
	@IBOutlet weak var viewLoader: UIView!
	@IBOutlet weak var mainStackviewNumber: UIStackView!
	
	// MARK: Life Cycle
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.setupAnimation()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(true)
		self.updateUI()
		self.viewLoader.isHidden = true
	}
	
	// MARK: Action
	
	@IBAction func tapClean(_ sender: Any) {
		Vibration.selection.vibrate()
		self.labelCardCVV.text = ""
		self.updateUI()
	}
	
	@IBAction func tapNext(_ sender: Any) {
		Vibration.selection.vibrate()
		self.viewLoader.isHidden = false
		UserStorage.newCard[UserStorage.newCardKeys.cvv.rawValue] = self.labelCardCVV.text
		APIFetcher.shared.request(.newCard([.number:UserStorage.newCard[UserStorage.newCardKeys.number.rawValue], .expiration: UserStorage.newCard[UserStorage.newCardKeys.expiration.rawValue],  .cvv: UserStorage.newCard[UserStorage.newCardKeys.cvv.rawValue]]), delegate: self) { (response) in
			self.responseCreateCard(response: response)
		}
	}
	
	@IBAction func tapRemoveChar(_ sender: Any) {
		Vibration.selection.vibrate()
		self.labelCardCVV.text = String(self.labelCardCVV.text?.dropLast() ?? "")
		self.updateUI()
	}
	
	@IBAction func tapBack(_ sender: Any) {
		self.dismiss(animated: true, completion: nil)
	}
	
	// MARK: Function
	
	func setupAnimation() {
		self.mainStackviewNumber.arrangedSubviews.forEach { (s) in
			(s as? UIStackView)?.arrangedSubviews.forEach({ (button) in
				if (button as? UIButton)?.titleLabel?.text != "x" && (button as? UIButton)?.titleLabel?.text != ">" {
					(button as? UIButton)?.addTarget(self, action: #selector(self.clicBtn), for: .touchUpInside)
				}
			})
		}
	}
	
	func addCharactereToCardLabel(char: String) {
		self.labelCardCVV.text = (self.labelCardCVV.text ?? "") + char
	}
	
	func updateUI() {
		UIView.animate(withDuration: 0.4, animations: {
			let clic = (self.labelCardCVV.text ?? "").count == self.maxLengthCCV
			self.mainStackviewNumber.arrangedSubviews.forEach { (s) in
				(s as? UIStackView)?.arrangedSubviews.forEach({ (button) in
					if let b = button as? UIButton, b.titleLabel?.text != "x", b.titleLabel?.text != ">" {
						b.isUserInteractionEnabled = !clic
						b.setTitleColor(!clic ? UIColor.label : UIColor.secondaryLabel, for: .normal)
					}
				})
			}
			self.labelPlaceholder.isHidden = !(self.labelCardCVV.text?.count == 0)
		})
	}
	
	// MARK: Object function
	
	@objc func clicBtn(button: UIButton) {
		Vibration.selection.vibrate()
		self.addCharactereToCardLabel(char: (button.titleLabel?.text)!)
		self.updateUI()
		UIView.animate(withDuration: 0.1, delay: 0, options: .allowUserInteraction, animations: {
			button.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
			
		}) { _ in
			UIView.animate(withDuration: 0.1, delay: 0, options: .allowUserInteraction, animations: {
				button.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
			}) { _ in
				UIView.animate(withDuration: 0.1, delay: 0, options: .allowUserInteraction, animations: {
					button.transform = CGAffineTransform.identity
				})
			}
		}
	}
	
	// MARK: Request delegate
	
	func responseCreateCard(response : Error?) {
		self.viewLoader.isHidden = true
		if let _ = response {
			return
		} else {
			AuthManager.shared.user.cards = APIFetcher.shared.allCached()
			FirestoreManager.shared.updateCardOfUser { (error) in
				if error != nil { self.showAlert(title: "Une erreur est survenue.", description: error?.localizedDescription ?? "Veuillez réessayer.") }
				else {
					UserStorage.newCard = [:]
					let mainStoryboard = UIStoryboard(name: StoryboardName.main.rawValue, bundle: nil).instantiateViewController(identifier: ControllerName.home.rawValue) as! HomeController
					let nav = UIStoryboard(name: StoryboardName.main.rawValue, bundle: nil).instantiateInitialViewController() as! UINavigationController
					nav.viewControllers = [mainStoryboard]
					UIApplication.shared.windows.first?.rootViewController = nav
				}
			}
		}
	}
	
	func returnCacheRequest(completion: Error?) {
		self.responseCreateCard(response: completion)
	}
}
