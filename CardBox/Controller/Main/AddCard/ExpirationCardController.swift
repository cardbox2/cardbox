//
//  ExpirationCardController.swift
//  CardBox
//
//  Created by Thomas Gallier on 13/04/2021.
//

import Foundation
import UIKit

class ExpirationCardController:UIViewController {
	
	// MARK: Variable
	
	let maxLengthExpiration = 5
	
	// MARK: Outlet
	
	@IBOutlet weak var labelPlaceholder: UILabel!
	@IBOutlet weak var btnReset: UIButton!
	@IBOutlet weak var btnNext: UIButton!
	@IBOutlet weak var labelCardExpiration: UILabel!
	@IBOutlet weak var mainStackviewNumber: UIStackView!
	
	// MARK: Life Cycle
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.setupAnimation()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(true)
		self.updateUI()
	}
	
	// MARK: Action
	
	@IBAction func tapClean(_ sender: Any) {
		Vibration.selection.vibrate()
		self.labelCardExpiration.text = ""
		self.updateUI()
	}
	
	@IBAction func tapNext(_ sender: Any) {
		Vibration.selection.vibrate()
		UserStorage.newCard[UserStorage.newCardKeys.expiration.rawValue] = self.labelCardExpiration.text
		if self.removeSpaces(text: self.labelCardExpiration.text ?? "").count == self.maxLengthExpiration {
			let cvvCardController = self.storyboard?.instantiateViewController(identifier: ControllerName.cvvCard.rawValue) as! CVVCardController
			cvvCardController.modalPresentationStyle = .fullScreen
			cvvCardController.modalTransitionStyle = .crossDissolve
			self.present(cvvCardController, animated: false, completion: nil)
		}
	}
	
	@IBAction func tapRemoveChar(_ sender: Any) {
		Vibration.selection.vibrate()
		self.labelCardExpiration.text = String(self.labelCardExpiration.text?.dropLast() ?? "")
		if self.labelCardExpiration.text?.last == " " || self.labelCardExpiration.text?.last == "/" {
			self.tapRemoveChar(sender)
		}
		self.updateUI()
	}
	
	@IBAction func tapBack(_ sender: Any) {
		self.dismiss(animated: true, completion: nil)
	}
	
	// MARK: Function
	
	func setupAnimation() {
		self.mainStackviewNumber.arrangedSubviews.forEach { (s) in
			(s as? UIStackView)?.arrangedSubviews.forEach({ (button) in
				if (button as? UIButton)?.titleLabel?.text != "x" && (button as? UIButton)?.titleLabel?.text != ">" {
					(button as? UIButton)?.addTarget(self, action: #selector(self.clicBtn), for: .touchUpInside)
				}
			})
		}
	}
	
	func removeSpaces(text: String) -> String {
		return String(text).replacingOccurrences(of: " ", with: "")
	}
	
	func addCharactereToCardLabel(char: String) {
		self.labelCardExpiration.text = (self.labelCardExpiration.text ?? "") + char
	}
	
	func updateUI() {
		UIView.animate(withDuration: 0.4, animations: {
			let clic = self.removeSpaces(text: self.labelCardExpiration.text ?? "").count == self.maxLengthExpiration
			self.mainStackviewNumber.arrangedSubviews.forEach { (s) in
				(s as? UIStackView)?.arrangedSubviews.forEach({ (button) in
					if let b = button as? UIButton, b.titleLabel?.text != "x", b.titleLabel?.text != ">" {
						b.isUserInteractionEnabled = !clic
						b.setTitleColor(!clic ? UIColor.label : UIColor.secondaryLabel, for: .normal)
					}
				})
			}
			self.labelPlaceholder.isHidden = !(self.labelCardExpiration.text?.count == 0)
		})
	}
	
	// MARK: Object function
	
	@objc func clicBtn(button: UIButton) {
		Vibration.selection.vibrate()
		if self.removeSpaces(text: self.labelCardExpiration.text ?? "").count == 2 {
			self.addCharactereToCardLabel(char: " / ")
		}
		self.addCharactereToCardLabel(char: (button.titleLabel?.text)!)
		self.updateUI()
		UIView.animate(withDuration: 0.1, delay: 0, options: .allowUserInteraction, animations: {
			button.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
			
		}) { _ in
			UIView.animate(withDuration: 0.1, delay: 0, options: .allowUserInteraction, animations: {
				button.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
			}) { _ in
				UIView.animate(withDuration: 0.1, delay: 0, options: .allowUserInteraction, animations: {
					button.transform = CGAffineTransform.identity
				})
			}
		}
	}
}
