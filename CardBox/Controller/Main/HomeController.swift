//
//  HomeController.swift
//  CardBox
//
//  Created by Thomas Gallier on 12/04/2021.
//

import Foundation
import UIKit
import FirebaseAuth
import FirebaseFirestore
import FirebaseStorage

extension HomeController:UITableViewDelegate, UITableViewDataSource {
	
	// Data come from FakeData.json
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		self.heightTableView.constant = CGFloat(self.cardPayments.count * 80)
		return self.cardPayments.count
	}
	
	func numberOfSections(in tableView: UITableView) -> Int { return 1 }
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat { return 80 }
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell:UITableViewCell?
		cell = self.tableView.dequeueReusableCell(withIdentifier: "PaymentCell")
		(cell as? PaymentCellModel)?.configuration(data: self.cardPayments[indexPath.row])
		cell?.selectionStyle = .none
		return cell ?? UITableViewCell()
	}
}

extension HomeController:UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
	// MARK: Collection view cards
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return 1 + AuthManager.shared.user.cards.count
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell:UICollectionViewCell?
		
		if (indexPath.row == 0) {
			cell = CollectionKey.ADD_CARD.dequeue(collectionView: collectionView, indexPath: indexPath)
		} else {
			cell = CollectionKey.CARD.dequeue(collectionView: collectionView, indexPath: indexPath)
			(cell as? CardCellModel)?.configuration(data: AuthManager.shared.user.cards[indexPath.row - 1])
		}
		return cell ?? UICollectionViewCell()
	}
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		if (indexPath.row == 0) {
			return CollectionKey.ADD_CARD.size(collectionView: collectionView)
		} else {
			return CollectionKey.CARD.size(collectionView: collectionView)
		}
	}
	
	func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
		if (indexPath.row == 1) { self.didLaunch ? nil : self.initScrollToItem() }
	}
	
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		if (indexPath.row == 0) { self.openAddCard() }
		else { self.openDetailCard(row: indexPath.row - 1) }
	}
}

extension HomeController {
	func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
		let cellItemWidth = widthCard
		let pageWidth = Float(cellItemWidth + 16)
		let offsetXAfterDragging = Float(scrollView.contentOffset.x)
		let targetOffsetX = Float(targetContentOffset.pointee.x)
		let pagesCountForOffset = pagesCount(forOffset: offsetXAfterDragging, withTargetOffset: targetOffsetX, pageWidth: pageWidth)
		var newTargetOffsetX = pagesCountForOffset * pageWidth
		keepNewTargetInBounds(&newTargetOffsetX, scrollView)
		targetContentOffset.pointee.x = CGFloat(offsetXAfterDragging)
		let newTargetPoint = CGPoint(x: CGFloat(newTargetOffsetX), y: scrollView.contentOffset.y)
		scrollView.setContentOffset(newTargetPoint, animated: true)
	}
	
	fileprivate func pagesCount(forOffset offset: Float, withTargetOffset targetOffset: Float, pageWidth: Float) -> Float {
		let isRightDirection = targetOffset > offset
		let roundFunction = isRightDirection ? ceilf : floorf
		let pagesCountForOffset = roundFunction(offset / pageWidth)
		return pagesCountForOffset
	}
	
	fileprivate func keepNewTargetInBounds(_ newTargetOffsetX: inout Float, _ scrollView: UIScrollView) {
		if newTargetOffsetX < 0 { newTargetOffsetX = 0 }
		let contentSizeWidth = Float(scrollView.contentSize.width)
		if newTargetOffsetX > contentSizeWidth { newTargetOffsetX = contentSizeWidth }
	}
}

extension HomeController {
	enum CollectionKey:String, CaseIterable{
		case ADD_CARD, CARD
		
		func cellName() -> String{
			switch self {
				case .CARD:
					return "CardCell"
				case .ADD_CARD:
					return "AddCardCell"
			}
		}
		
		func register(collectionView: UICollectionView) {
			collectionView.register(UINib(nibName: self.cellName(), bundle: nil), forCellWithReuseIdentifier: self.cellName())
		}
		
		func dequeue(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
			collectionView.dequeueReusableCell(withReuseIdentifier: self.cellName(), for: indexPath)
		}
		
		func size(collectionView: UICollectionView) -> CGSize {
			switch self {
				case .CARD:
					return CGSize(width: UIScreen.main.bounds.width - 64, height: collectionView.bounds.height)
				case .ADD_CARD:
					return CGSize(width: UIScreen.main.bounds.width - 64, height: collectionView.bounds.height)
			}
		}
	}
}

class HomeController:UIViewController {
		
	// MARK: Outlet
	
	@IBOutlet weak var heightTableView: NSLayoutConstraint!
	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var stackViewTitleSettings: UIStackView!
	@IBOutlet weak var collectionViewCards: UICollectionView!
	
	// MARK: Variable
	
	let widthCard = UIScreen.main.bounds.width - 64
	var didLaunch = false
	var cards = [Card]()
	var cardPayments = [CardPayment]()
	
	// MARK: Life Cycle
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.registerNib()
		self.addGesture()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(true)
		self.loadCards()
	}
	
	// MARK: Function
	
	func loadCards() {
		if !(AuthManager.shared.user?.cards.isEmpty ?? false) {
			self.cardPayments = Model.shared.allCached().sorted(by: { (a, b) -> Bool in
				return a.id > b.id
			})
		} else {
			self.cardPayments = []
		}
		AuthManager.shared.user?.cards = APIFetcher.shared.allCached()
		self.collectionViewCards.reloadData()
	}
	
	func addGesture() {
		let gestureTapSettings = UITapGestureRecognizer(target: self, action: #selector(self.openSettings))
		gestureTapSettings.numberOfTapsRequired = 1
		gestureTapSettings.numberOfTouchesRequired = 1
		self.stackViewTitleSettings.addGestureRecognizer(gestureTapSettings)
	}
	
	func registerNib() {
		self.tableView.register(UINib(nibName: "PaymentCell", bundle: nil), forCellReuseIdentifier: "PaymentCell")
		CollectionKey.CARD.register(collectionView: collectionViewCards)
	}
	
	func initScrollToItem() {
		self.collectionViewCards?.scrollToItem(at: IndexPath(row: 1, section: 0), at: .centeredHorizontally, animated: true)
		self.didLaunch = true
	}
	
	
	func openAddCard() {
		Vibration.selection.vibrate()
		let addCardController = self.storyboard?.instantiateViewController(identifier: ControllerName.addCard.rawValue) as! AddCardController
		addCardController.modalPresentationStyle = .fullScreen
		addCardController.modalTransitionStyle = .coverVertical
		self.present(addCardController, animated: true, completion: nil)
	}
	
	func openDetailCard(row: Int) {
		Vibration.selection.vibrate()
		let openDetailController = self.storyboard?.instantiateViewController(identifier: ControllerName.detailCard.rawValue) as! DetailsCardController
		openDetailController.card = AuthManager.shared.user.cards[row]
		self.navigationController?.pushViewController(openDetailController, animated: true)
	}
	
	// MARK: Object function
	
	@objc func openSettings() {
		Vibration.selection.vibrate()
		let settingsController = self.storyboard?.instantiateViewController(identifier: ControllerName.settings.rawValue) as! SettingsController

		settingsController.modalPresentationStyle = .fullScreen
		settingsController.modalTransitionStyle = .coverVertical
		self.present(settingsController, animated: true, completion: nil)
	}
	
}


