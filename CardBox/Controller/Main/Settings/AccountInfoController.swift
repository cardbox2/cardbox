//
//  AccountInfoController.swift
//  CardBox
//
//  Created by Thomas Gallier on 14/04/2021.
//

import Foundation
import UIKit

class AccountInfoController:UIViewController {
	
	// MARK: Outlet
	
	@IBOutlet weak var textFieldName: UITextField!
	@IBOutlet weak var textFieldBirthday: UITextField!
	@IBOutlet weak var textFieldEmail: UITextField!
	@IBOutlet weak var btnBack: UIButton!
	
	// MARK: Life Cycle
	
	override func viewDidLoad() {
		super.viewDidLoad()
		navigationController?.interactivePopGestureRecognizer?.delegate = nil
		self.hideKeyboardWhenTappedAround()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(true)
		self.setupInfo()
	}
	
	// MARK: Action
	
	@IBAction func tapBack(_ sender: Any) {
		self.dismiss(animated: true, completion: nil)
	}
	
	// MARK: Function
	
	func setupInfo() {
		let u = AuthManager.shared.user
		self.textFieldEmail.text = u?.email
		self.textFieldName.text = u?.name
		self.textFieldBirthday.text = u?.birthday
	}
	
	
}
