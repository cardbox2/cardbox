//
//  SettingsController.swift
//  CardBox
//
//  Created by Thomas Gallier on 12/04/2021.
//

import Foundation
import UIKit

class SettingsController:UIViewController {
	
	// MARK: Outlet
	
	@IBOutlet weak var viewInfo: UIView!
	@IBOutlet weak var viewSignOut: UIView!
	
	// MARK: Life Cycle
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.setupGesture()
	}
	
	// MARK: Action
	
	@IBAction func tapBack(_ sender: Any) {
		self.dismiss(animated: true, completion: nil)
	}
	
	// MARK: Function
	
	func setupGesture() {
		let tapSignOut = UITapGestureRecognizer(target: self, action: #selector(self.tapSignOut))
		tapSignOut.numberOfTouchesRequired = 1
		tapSignOut.numberOfTapsRequired = 1
		
		let tapInfo = UITapGestureRecognizer(target: self, action: #selector(self.tapInfo))
		tapInfo.numberOfTouchesRequired = 1
		tapInfo.numberOfTapsRequired = 1
		
		self.viewSignOut.addGestureRecognizer(tapSignOut)
		self.viewInfo.addGestureRecognizer(tapInfo)
	}
	
	// MARK: Object functions
	
	@objc func tapSignOut() {
		Vibration.light.vibrate()
		AuthManager.shared.signOut()
		APIFetcher.shared.resetCache()
	}
	
	@objc func tapInfo() {
		Vibration.light.vibrate()
		let infoController = self.storyboard?.instantiateViewController(identifier: ControllerName.accountInfo.rawValue) as! AccountInfoController
		infoController.modalPresentationStyle = .fullScreen
		infoController.modalTransitionStyle = .coverVertical
		self.present(infoController, animated: true, completion: nil)
	}
	
	
}


