//
//  DetailsCardController.swift
//  CardBox
//
//  Created by Thomas Gallier on 13/04/2021.
//

import Foundation
import UIKit

extension DetailsCardController {
	
	func registerNib() {
		self.tableView.register(UINib(nibName: "PaymentCell", bundle: nil), forCellReuseIdentifier: "PaymentCell")
	}
	
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return self.cardPayments.count
	}
	
	override func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}
	
	override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 80
	}
	
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell:UITableViewCell?
		cell = self.tableView.dequeueReusableCell(withIdentifier: "PaymentCell")
		(cell as? PaymentCellModel)?.configuration(data: self.cardPayments[indexPath.row])
		cell?.selectionStyle = .none
		return cell ?? UITableViewCell()
	}
}

class DetailsCardController: UITableViewController, APIFetcherDelegate {
	
	// MARK: Variable
	
	var card:Card!
	var cardPayments = [CardPayment]()
	// MARK: Outlet
	@IBOutlet weak var btnBack: UIButton!
	@IBOutlet weak var viewCard: UIView!
	@IBOutlet weak var labelCardNumber: UILabel!
	@IBOutlet weak var labelCardExpiration: UILabel!
	@IBOutlet weak var btnTrash: LoadingButton!
	
	// MARK: Life Cycle
	
	override func viewDidLoad() {
		super.viewDidLoad()
		navigationController?.interactivePopGestureRecognizer?.delegate = nil
		self.registerNib()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(true)
		self.cardPayments = Model.shared.allCached().sorted(by: { (a, b) -> Bool in
			return a.id > b.id
		})
	}
	
	override func viewWillLayoutSubviews() {
		super.viewWillLayoutSubviews()
		self.setupUI()
	}
	
	// MARK: Action
	
	@IBAction func tapBack(_ sender: Any) {
		self.navigationController?.popViewController(animated: true)
	}
	
	@IBAction func tapDelete(_ sender: Any) {
		Vibration.selection.vibrate()
		self.btnTrash.showLoading()
		APIFetcher.shared.request(.deleteCard(self.card.id), delegate: self) { (response) in
			self.responseDeleteCard(response: response)
		}
	}
	// MARK: Function

	func setupUI() {
		if (self.viewCard.layer.sublayers?.first is CAGradientLayer) {
			self.viewCard.layer.sublayers?.first?.removeFromSuperlayer()
		}
		self.viewCard.applyGradient(startPoint: CGPoint(x: 0, y: 0), endPoint: CGPoint(x: 1, y: 1), radius: 32, colours:  [UIColor.red,UIColor.pink, UIColor.blue])
		self.labelCardNumber.text = self.card.number
		self.labelCardExpiration.text = self.card.expiration
	}
	
	// MARK: Request delegate
	
	func responseDeleteCard(response : Error?) {
		self.btnTrash.hideLoading()
		if let _ = response {
			return
		} else {
			APIFetcher.shared.removeFromCache(ofType: Card.self, withId: self.card.id)
			AuthManager.shared.user.cards = APIFetcher.shared.allCached()
			FirestoreManager.shared.updateCardOfUser { (error) in
				if error != nil { self.showAlert(title: "Une erreur est survenue.", description: error?.localizedDescription ?? "Veuillez réessayer.") }
				else {
					self.navigationController?.popViewController(animated: true)
				}
			}
		}
	}
	
	func returnCacheRequest(completion: Error?) {
		self.responseDeleteCard(response: completion)
	}
}
