//
//  SplashScreenController.swift
//  CardBox
//
//  Created by Thomas Gallier on 14/04/2021.
//

import Foundation
import UIKit
import FirebaseAuth

class SplashScreenController:UIViewController, APIFetcherDelegate {
	
	// MARK: Life Cycle
	
	override func viewDidLoad() {
		super.viewDidLoad()
		DispatchQueue.main.async {
			self.instantiateMainStoryboard()
		}
	}
		
	// MARK: Function
	
	func instantiateMainStoryboard() {
		if AuthManager.shared.user == nil {
			if let u = Auth.auth().currentUser {
				FirestoreManager.shared.getCurrentUserDoc(uid: u.uid) { result in
					switch result {
						case .failure(_):
							self.goOnboarding()
						case .success(_):
							self.request()
					}
				}
			} else { self.goOnboarding() }
		} else { self.goOnboarding() }
	}
	
	func goOnboarding() {
		let mainStoryboard = self.storyboard?.instantiateViewController(identifier: ControllerName.signMethod.rawValue) as! SignMethodController
		let nav = UIStoryboard(name: StoryboardName.boarding.rawValue, bundle: nil).instantiateInitialViewController() as! UINavigationController
		nav.viewControllers = [mainStoryboard]
		UIApplication.shared.windows.first?.rootViewController = nav
	}
	
	
	func goMain() {
		let mainStoryboard = UIStoryboard(name: StoryboardName.main.rawValue, bundle: nil).instantiateViewController(identifier: ControllerName.home.rawValue) as! HomeController
		let nav = UIStoryboard(name: StoryboardName.main.rawValue, bundle: nil).instantiateInitialViewController() as! UINavigationController
		nav.viewControllers = [mainStoryboard]
		UIApplication.shared.windows.first?.rootViewController = nav
	}
	
	// MARK: Request delegate
	
	func request() {
		APIFetcher.shared.request(.cards, delegate: self) { (response) in
			self.responseFetchCard(response: response)
		}
	}
	
	func responseFetchCard(response : Error?) {
		if let _ = response {
			return
		} else {
			AuthManager.shared.user.cards = APIFetcher.shared.allCached()
			goMain()
		}
	}
	
	func returnCacheRequest(completion: Error?) {
		self.responseFetchCard(response: completion)
	}
	
}
