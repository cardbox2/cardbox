//
//  UserDefault.swift
//  CardBox
//
//  Created by Thomas Gallier on 13/04/2021.
//

import Foundation
import UIKit

// --------- KEY ----- //

extension Key {
	static let newUser:Key = "newUser"
	static let newCard:Key = "newCard"
}

struct UserStorage {
	
	@UserDefault(.newUser, defaultValue: [String:String]())
	static var newUser:[String:String]
	
	@UserDefault(.newCard, defaultValue: [String:String]())
	static var newCard:[String:String]
	
	enum newUserKeys:String {
		case _id
		case name
		case email
		case birthday
		case sign_up_with
	}
	
	enum newCardKeys:String {
		case _id
		case number
		case expiration
		case cvv
	}
}


// --------- WRAPPER ------- //


struct Key: RawRepresentable, Equatable {
	let rawValue: String
}

extension Key: ExpressibleByStringLiteral {
	init(stringLiteral: String) { rawValue = stringLiteral }
}

// User Default Property Wrapper
// The marker protocol
protocol DefaultsSerializable {}

extension Data: DefaultsSerializable {}
extension String: DefaultsSerializable {}
extension Date: DefaultsSerializable {}
extension Bool: DefaultsSerializable {}
extension Int: DefaultsSerializable {}
extension Double: DefaultsSerializable {}
extension Float: DefaultsSerializable {}

extension Array: DefaultsSerializable where Element: DefaultsSerializable {}
extension Dictionary: DefaultsSerializable where Key == String, Value: DefaultsSerializable {}

@propertyWrapper
struct UserDefault<T> where T: DefaultsSerializable {
	let key: Key
	let defaultValue: T
	
	init(_ key: Key, defaultValue: T) {
		self.key = key
		self.defaultValue = defaultValue
	}
	
	var wrappedValue: T {
		get {
			switch key {
				case .newUser:
					return UserDefaults.standard.object(forKey: key.rawValue) as? T ?? defaultValue
				default:
					return UserDefaults.standard.object(forKey: key.rawValue + "-/-" + AuthManager.shared.user!.id) as? T ?? defaultValue
			}
		}
		set {
			switch key {
				case .newUser:
					UserDefaults.standard.set(newValue, forKey: key.rawValue)
				default:
					UserDefaults.standard.set(newValue, forKey: key.rawValue + "-/-" + AuthManager.shared.user!.id)
			}
		}
	}
	
	
}
