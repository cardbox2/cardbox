//
//  AuthManager.swift
//  CardBox
//
//  Created by Thomas Gallier on 14/04/2021.
//

import Foundation
import FirebaseAuth

class AuthManager:NSObject {
	
	static let shared = AuthManager()
	private override init() { super.init() }
	
	var user:User!
	
	func signUp(email: String, password: String, completion: @escaping (Error?) -> Void) {
		Auth.auth().createUser(withEmail: email, password: password) { (result, error) in
			if let e = error { completion(e) }
			if let r = result {
				UserStorage.newUser[UserStorage.newUserKeys._id.rawValue] = r.user.uid
				FirestoreManager.shared.createUserDoc(uid: r.user.uid, data: UserStorage.newUser) { err in
					if err != nil { completion(err) }
					else {
						var d = UserStorage.newUser
						d["signUpDate"] = Date().toStringDate()
						self.user = User(dict: d)
						print("Sign up user")
						completion(nil)
					}
				}
			} else { completion(EmbuscadeError.unknow) }
		}
	}
	
	func signIn(email: String, password: String, completion: @escaping (Error?) -> Void) {
		Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
			if let e = error { completion(e) }
			if let r = result {
				FirestoreManager.shared.getCurrentUserDoc(uid: r.user.uid) { (result) in
					switch result {
						case .success(let d): completion(nil)
						case .failure(let e): completion(e)
					}
				}
			} else { completion(EmbuscadeError.unknow) }
		}
	}
	
	func signOut() {
		try? Auth.auth().signOut()
		self.user = nil
		let mainStoryboard = UIStoryboard(name: StoryboardName.boarding.rawValue, bundle: nil).instantiateViewController(identifier: ControllerName.signMethod.rawValue) as! SignMethodController
		let nav = UIStoryboard(name: StoryboardName.boarding.rawValue, bundle: nil).instantiateInitialViewController() as! UINavigationController
		nav.viewControllers = [mainStoryboard]
		UIApplication.shared.windows.first?.rootViewController = nav
	}
	
}
