//
//  FirestoreManager.swift
//  CardBox
//
//  Created by Thomas Gallier on 14/04/2021.
//

import Foundation
import FirebaseFirestore

class FirestoreManager:NSObject {
	
	static let shared = FirestoreManager()
	var firestore:Firestore!
	
	private override init() {
		let settings = FirestoreSettings()
		settings.isPersistenceEnabled = true
		self.firestore = Firestore.firestore()
		self.firestore?.settings = settings
		super.init()
	}
	
	enum CollectioKeys:String{ case users }
	
	func getCollection(_ key : CollectioKeys) -> CollectionReference {
		return self.firestore.collection(key.rawValue)
	}
	
	func getCurrentUserDoc(uid:String, completion:@escaping (Result<[String:Any], Error>) -> Void){
		self.getCollection(.users).document(uid).getDocument { (doc, error) in
			if let e = error { completion(.failure(e)) }
			else {
				if let d = doc?.data() {
					print("User loaded")
					AuthManager.shared.user = User(dict: d)
					completion(.success(doc?.data() ?? [:]))
				} else { completion(.failure(EmbuscadeError.unknow)) }
			}
		}
	}
	
	func updateCardOfUser(completion:@escaping (Error?) -> Void) {
		let cards:[Card] = APIFetcher.shared.allCached()
		let ids = cards.map({ (card: Card) -> String in card.id })
		self.getCollection(.users).document(AuthManager.shared.user.id).getDocument { (doc, error) in
			if let e = error { completion(e) }
			else {
				doc?.reference.updateData(["cards": ids]) { (error) in
					print("Card updated")
					completion(error)
				}
			}
		}
	}
	
	func createUserDoc(uid: String, data: [String:Any], completion:@escaping (Error?) -> Void) {
		self.getCollection(.users).document(uid).setData(data) { (error) in
			completion(error)
		}
	}
	
}
