//
//  FeedFetcherService.swift
//  Picdies
//
//  Created by Jude on 16/02/2019.
//  Copyright © 2019 streamlabs. All rights reserved.
//

import Foundation
import SwiftyJSON


protocol APIFetcherDelegate {
	func returnCacheRequest(completion: Error?)
}

class APIFetcher: NSObject {
	
	static let shared = APIFetcher()
	private override init() { super.init() }
	
	// MARK : Network task
	
	var cacheRequest = [(APIRequest,APIFetcherDelegate)]()
	
	func request(_ request : APIRequest, delegate : APIFetcherDelegate, completion: @escaping (Error?) -> Void) {
		APICall.shared.loadFromRequest(request) { [self] (response) in
			self.cacheRequest.append((request,delegate))
			switch response {
				case .success(let data) :
					let json = JSON(data)
					if json[BaseObject.keys.id.rawValue].string == nil {
						for (_, value) in json {
							_ = self.generateCachedObject(ofType: request.responseType()!, withDict: value.dictionaryObject ?? [:])
						}
					} else {
						_ = self.generateCachedObject(ofType: request.responseType()!, withDict: json.dictionaryObject ?? [:])
					}
					self.cacheRequest.removeAll{$0.0.url() == request.url()}
					DispatchQueue.main.async { completion(nil) }
				case .failure(let error) :
					print(error.localizedDescription)
					if self.cacheRequest.filter({ (cache) -> Bool in cache.0.url() == request.url() }).count < 3 {
						self.request(request, delegate: delegate, completion: { _ in completion(error) })
					} else { self.cacheRequest.removeAll{$0.0.url() == request.url()} }
					DispatchQueue.main.async { completion(error) }
			}
		}
	}
	
	func requestJSON(_ request : APIRequest, delegate : APIFetcherDelegate, completion: @escaping (JSON?, Error?) -> Void) {
		APICall.shared.loadFromRequest(request) { [self] (response) in
			switch response {
				case .success(let data) :
					DispatchQueue.main.async { completion(JSON(data), nil) }
				case .failure(let error) :
					DispatchQueue.main.async { completion(nil, error) }
			}
		}
	}
	
	// MARK: - Cache Management
	
	private var cache = [String: [String: BaseObject]]()
	private var cacheIsLocked = false
	private var cacheQueue = [BaseObject]()
	
	private func cache(_ object: BaseObject) {
		self.cacheQueue.append(object)
		self.nextCacheAdd()
	}
	
	private func nextCacheAdd() {
		if !self.cacheQueue.isEmpty && !self.cacheIsLocked {
			self.cacheIsLocked = true
			let object = self.cacheQueue.removeFirst()
			let name = String(describing: type(of: object).self)
			if self.cache[name] != nil { self.cache[name]![object.id] = object } else { self.cache[name] = [object.id: object] }
			self.cacheIsLocked = false
			self.nextCacheAdd()
		}
	}
	
	// MARK: - Public
	func generateCachedObject<T: BaseObject>(ofType type: T.Type, withDict dict: [String: Any]) -> T {
		let id: String
		if let i = dict[BaseObject.keys.id.rawValue] as? String { id = i } else { id = String.random() }
		let object: T
		if id == AuthManager.shared.user?.id {
			AuthManager.shared.user?.setupWith(dict: dict)
		}
		if let o = self.getCachedObject(ofType: type, withId: id) as? T {
			object = o
			DispatchQueue.main.async { object.setupWith(dict: dict) }
		} else { object = type.init(dict: dict) }
		self.cache(object)
		return object
	}
	
	func resetCache() { self.cache.removeAll() }
	
	func getCachedObject(ofType type: AnyClass, withId id: String) -> BaseObject? {
		let name = String(describing: type.self)
		return self.cache[name]?[id]
	}
	
	func removeFromCache(ofType type:AnyClass, withId id:String) {
		let name = String(describing: type.self)
		if let index = self.cache[name]?.firstIndex(where: { (arg0) -> Bool in
			let (key, _) = arg0
			return key == id
		}) {
			self.cache[name]?.remove(at: index)
		}
	}
	
	func allCached<T: BaseObject>() -> [T] {
		let name = String(describing: T.self)
		var array: [T] = []
		if let data = self.cache[name] {
			for (_, v) in data {
				array.append(v as! T)
			}
		}
		return array.sorted { (a, b) -> Bool in
			return a.created_at ?? "" > b.created_at ?? ""
		}
	}
}

