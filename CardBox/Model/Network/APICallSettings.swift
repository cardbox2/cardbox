//
//  APICallSettings.swift
//  Picdies
//
//  Created by Thomas Gallier on 23/12/2020.
//  Copyright © 2020 streamlabs. All rights reserved.
//

import Foundation
import Alamofire

enum APIRequest {
	
	enum APIkeys : String {
		case base_url = "https://api-cardbox.herokuapp.com/"
	}
	
	enum APIKeysBody:String, DefaultsSerializable {
		case number
		case cvv
		case expiration
		case ids
		case id
	}
	
	case cards
	case card(String)
	case newCard([APIRequest.APIKeysBody : Any])
	case deleteCard(String)
	
	func name() -> String {
		switch self {
			case .cards:
				return "cards"
			case let .card(id), let .deleteCard(id):
				return "card/\(id)"
			case .newCard:
				return "card"
		}
	}
	
	func type() -> HTTPMethod {
		switch self {
			case .card: return .get
			case .newCard, .cards: return .post
			case .deleteCard: return .delete
		}
	}
	
	func contentType() -> HTTPHeaders? {
		switch self {
			default: return nil
		}
	}
	
	func responseType() -> BaseObject.Type? {
		switch self {
			case .card, .cards, .newCard, .deleteCard: return Card.self
		}
	}
	
	func body() -> [String:Any]? {
		switch self {
			case
				let .newCard(body)
			: return transformeBodyKey(body) as [String : Any]
			case .cards
			: return transformeBodyKey([APIRequest.APIKeysBody.ids: AuthManager.shared.user.cardsId]) as [String:Any]
			default: return nil
		}
	}
	
	func transformeBodyKey(_ body:[APIKeysBody:Any]) -> [String:Any?] {
		var array = [String:Any]()
		for (k,v) in body {
			array[k.rawValue] = v
		}
		return array
	}

	func url() -> String { return "\(APIkeys.base_url.rawValue)\(self.name())" }
	
}

public enum responseContent:String {
	case code
	case message
}
