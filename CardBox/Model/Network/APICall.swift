//
//  APICall.swift
//  Picdies
//
//  Created by Thomas Gallier on 23/12/2020.
//  Copyright © 2020 streamlabs. All rights reserved.
//

import Foundation
import Firebase
import SwiftyJSON
import Alamofire

class ModelAlamofire:NSObject {
	static let sharedManager: Session = {
		let configuration = URLSessionConfiguration.af.default
		configuration.timeoutIntervalForRequest = 10
		configuration.timeoutIntervalForResource = 10
		configuration.requestCachePolicy = .reloadRevalidatingCacheData
		configuration.waitsForConnectivity = true
		return Session(configuration : configuration)
	}()
	
	private override init() { super.init() }
}


class APICall:NSObject{
	
	static let shared = APICall()
	private override init() { super.init() }
	
	func loadFromRequest(_ url : APIRequest, completion : @escaping (Swift.Result<Data, Error>) -> Void) {
		print(url.url())
		ModelAlamofire.sharedManager.request(url.url(), method: url.type(), parameters: url.body(), encoding: JSONEncoding.default, headers: url.contentType()).validate().responseData { response in
			switch response.result {
				case .success(let data):
					completion(.success(data))
				case .failure(let error):
					completion(.failure(error))
			}
		}
	}
}

