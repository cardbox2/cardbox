//
//  Reachability.swift
//  Picdies
//
//  Created by Thomas Gallier on 24/12/2020.
//  Copyright © 2020 streamlabs. All rights reserved.
//

import Foundation
import Reachability

class NetworkManager: NSObject {
	
	var reachability: Reachability!
	
	static let shared: NetworkManager = { return NetworkManager() }()
	
	
	override init() {
		super.init()
		
		reachability = try? Reachability()
		
		NotificationCenter.default.addObserver(
			self,
			selector: #selector(networkStatusChanged(_:)),
			name: .reachabilityChanged,
			object: reachability
		)
		
		do {
			try reachability.startNotifier()
		} catch {
			print("Unable to start notifier")
		}
	}
	
	@objc func networkStatusChanged(_ notification: Notification) {	}
	
	static func stopNotifier() -> Void {
		do {
			try (NetworkManager.shared.reachability).startNotifier()
		} catch {
			print("Error stopping notifier")
		}
	}
	
	// Network is reachable
	static func isReachable(completed: @escaping (NetworkManager) -> Void) {
		if (NetworkManager.shared.reachability).connection != .unavailable {
			completed(NetworkManager.shared)
		}
	}
	
	// Network is unreachable
	static func isUnreachable(completed: @escaping (NetworkManager) -> Void) {
		if (NetworkManager.shared.reachability).connection == .unavailable {
			completed(NetworkManager.shared)
		}
	}
	
	// Network is reachable via WWAN/Cellular
	static func isReachableViaWWAN(completed: @escaping (NetworkManager) -> Void) {
		if (NetworkManager.shared.reachability).connection == .cellular {
			completed(NetworkManager.shared)
		}
	}
	
	// Network is reachable via WiFi
	static func isReachableViaWiFi(completed: @escaping (NetworkManager) -> Void) {
		if (NetworkManager.shared.reachability).connection == .wifi {
			completed(NetworkManager.shared)
		}
	}
}
