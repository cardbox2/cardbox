//
//  Card.swift
//  CardBox
//
//  Created by Thomas Gallier on 13/04/2021.
//

import Foundation

class Card:BaseObject  {
	
	enum keys:String { case number, cvv, expiration }
	
	var number:String?
	var cvv:String?
	var expiration:String?
	
	required init(dict: [String: Any]) { super.init(dict: dict) }
	
	override func setupWith(dict: [String: Any]) {
		super.setupWith(dict: dict)
		if let s = dict[keys.number.rawValue] as? String { self.number = s }
		if let s = dict[keys.cvv.rawValue] as? String { self.cvv = s }
		if let s = dict[keys.expiration.rawValue] as? String { self.expiration = s }
	}
}
