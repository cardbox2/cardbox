//
//  User.swift
//  CardBox
//
//  Created by Thomas Gallier on 13/04/2021.
//

import Foundation


class User:BaseObject  {
	
	enum keys:String { case name, birthday, sign_up_with, email, cards }
	enum signUpWithKeys:String { case email, phone }
	
	var name:String?
	var birthday:String?
	var sign_up_with:signUpWithKeys?
	var email:String?
	
	var cardsId = [String]()
	var cards = [Card]()

	required init(dict: [String: Any]) { super.init(dict: dict) }
	
	override func setupWith(dict: [String: Any]) {
		super.setupWith(dict: dict)
		if let s = dict[keys.name.rawValue] as? String { self.name = s }
		if let s = dict[keys.birthday.rawValue] as? String { self.birthday = s }
		if let s = dict[keys.sign_up_with.rawValue] as? String { self.sign_up_with = signUpWithKeys(rawValue: s) }
		if let s = dict[keys.email.rawValue] as? String { self.email = s }
		if let s = dict[keys.cards.rawValue] as? [String] { self.cardsId = s }
	}
}
