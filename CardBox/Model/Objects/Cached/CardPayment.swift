//
//  CardPayment.swift
//  CardBox
//
//  Created by Thomas Gallier on 15/04/2021.
//

import Foundation
import UIKit

class CardPayment:BaseObject  {
	
	enum keys:String { case color, type, name, price }
	enum TypeKeys:String { case food, balance, rent, sport }
	
	var type:TypeKeys?
	var name:String?
	var price:String?
	

	required init(dict: [String: Any]) { super.init(dict: dict) }
	
	override func setupWith(dict: [String: Any]) {
		super.setupWith(dict: dict)
		if let s = dict[keys.type.rawValue] as? String { self.type = TypeKeys(rawValue: s) }
		if let s = dict[keys.name.rawValue] as? String { self.name = s }
		if let s = dict[keys.price.rawValue] as? String { self.price = s }
	}
}
