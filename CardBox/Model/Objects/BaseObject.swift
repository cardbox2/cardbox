//
//  BaseObject.swift
//  CardBox
//
//  Created by Thomas Gallier on 13/04/2021.
//

import Foundation
import UIKit

import Foundation

class BaseObject:Comparable  {
	
	enum keys:String { case id = "_id", created_at = "createdAt", updated_at = "updatedAt" }
	
	let id: String
	var created_at, updated_at: String?
	
	required init(dict: [String: Any]) {
		if let s = dict[keys.id.rawValue] as? String { self.id = s }
		else { self.id = String.random() }
		self.setupWith(dict: dict)
	}
	
	func setupWith(dict: [String: Any]) {
		if let s = dict[keys.created_at.rawValue] as? String { self.created_at = s }
		if let s = dict[keys.updated_at.rawValue] as? String { self.updated_at = s }
	}
	
	
	static func < (lhs: BaseObject, rhs: BaseObject) -> Bool { return lhs.id < rhs.id }
	static func == (lhs: BaseObject, rhs: BaseObject) -> Bool { return lhs.id == rhs.id }
	
	
}
