//
//  Model.swift
//  CardBox
//
//  Created by Thomas Gallier on 13/04/2021.
//

import Foundation
import UIKit
import SwiftyJSON

class Model: NSObject {
	
	static let shared = Model()
	private override init() {
		super.init()
		self.loadFakeData()
	}
	
	// MARK: - Cache Management
	
	private var cache = [String: [String: BaseObject]]()
	private var cacheIsLocked = false
	private var cacheQueue = [BaseObject]()
	
	
	func loadFakeData() {
		if let path = Bundle.main.path(forResource: "FakeData", ofType: "json") {
			do {
				let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
				let json = JSON(data)
				for (_, value) in json {
					self.generateCachedObject(ofType: CardPayment.self, withDict: value.dictionaryObject ?? [:])
				}
			} catch {
				print("error while parse fake data")
			}
		} else {
			print("error can't find fake data")
		}
	}
	
	private func cache(_ object: BaseObject) {
		self.cacheQueue.append(object)
		self.nextCacheAdd()
	}
	
	private func nextCacheAdd() {
		if !self.cacheQueue.isEmpty && !self.cacheIsLocked {
			self.cacheIsLocked = true
			let object = self.cacheQueue.removeFirst()
			let name = String(describing: type(of: object).self)
			if self.cache[name] != nil { self.cache[name]![object.id] = object } else { self.cache[name] = [object.id: object] }
			self.cacheIsLocked = false
			self.nextCacheAdd()
		}
	}
	
	// MARK: - Public
	func generateCachedObject<T: BaseObject>(ofType type: T.Type, withDict dict: [String: Any]) -> T {
		let id: String
		if let i = dict[BaseObject.keys.id.rawValue] as? String { id = i } else { id = String.random() }
		let object: T
		if id == AuthManager.shared.user?.id { AuthManager.shared.user?.setupWith(dict: dict) }
		if let o = self.getCachedObject(ofType: type, withId: id) as? T {
			object = o
			DispatchQueue.main.async { object.setupWith(dict: dict) }
		} else { object = type.init(dict: dict) }
		self.cache(object)
		return object
	}
	
	func getCachedObject(ofType type: AnyClass, withId id: String) -> BaseObject? {
		let name = String(describing: type.self)
		return self.cache[name]?[id]
	}
	
	func allCached<T: BaseObject>() -> [T] {
		let name = String(describing: T.self)
		var array: [T] = []
		if let data = self.cache[name] {
			for (_, v) in data { array.append(v as! T) }
		}
		return array
	}
	
}
