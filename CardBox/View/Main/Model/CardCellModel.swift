//
//  CardViewModel.swift
//  CardBox
//
//  Created by Thomas Gallier on 12/04/2021.
//

import Foundation
import UIKit

class CardCellModel:UICollectionViewCell{
	
	// MARK: Variable
	
	var card:Card!
	
	// MARK: Outlet
	
	@IBOutlet weak var labelExpiration: UILabel!
	@IBOutlet weak var labelNumber: UILabel!
	
	// MARK: Life cycle
	
	override func prepareForReuse() {
		super.prepareForReuse()
	}
	
	// MARK: Function
	
	func configuration(data: Card) {
		self.card = data
		self.configurationData()
		self.configurationUI()
	}
	
	func configurationData() {
		self.labelNumber.text = self.card.number
		self.labelExpiration.text = self.card.expiration
	}
	
	func configurationUI() {
		self.layer.cornerRadius = 32
		self.applyGradient(startPoint: CGPoint(x: 0, y: 0), endPoint: CGPoint(x: 1, y: 1), radius: 32, colours:  [UIColor.red,UIColor.pink, UIColor.blue])
	}
}
