//
//  PaymentCellModel.swift
//  CardBox
//
//  Created by Thomas Gallier on 15/04/2021.
//

import Foundation
import UIKit

class PaymentCellModel:UITableViewCell{

	// MARK: Outlet
	
	@IBOutlet weak var labelDate: UILabel!
	@IBOutlet weak var imageType: UIImageView!
	@IBOutlet weak var viewAroundType: UIView!
	@IBOutlet weak var labelDescription: UILabel!
	@IBOutlet weak var labelPrice: UILabel!
	
	// MARK: Variable
	
	var payment:CardPayment!
	
	// MARK: Life cycle
	
	override func prepareForReuse() {
		super.prepareForReuse()
	}
	
	// MARK: Function
	
	func configuration(data: CardPayment) {
		self.payment = data
		self.configurationData()
	}
	
	func getTypeImage() -> UIImage {
		switch self.payment.type {
			case .food:
				return UIImage(systemName: "drop.fill")!
			case .balance:
				return UIImage(systemName: "case")!
			case .rent:
				return UIImage(systemName: "house")!
			case .sport:
				return UIImage(systemName: "sportscourt")!
			case .none:
				return UIImage()
		}
	}
	
	func configurationData() {
		self.labelDate.text = self.payment.created_at
		self.labelDescription.text = self.payment.name
		self.labelPrice.text = self.payment.price
		self.viewAroundType.applyGradient(startPoint: CGPoint(x: 0, y: 0), endPoint: CGPoint(x: 1, y: 1), radius: 16, colours:  [UIColor.red,UIColor.pink, UIColor.blue])
		self.imageType.image = self.getTypeImage()
	}
}
