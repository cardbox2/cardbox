//
//  Gradient.swift
//  CardBox
//
//  Created by Thomas Gallier on 13/04/2021.
//

import Foundation
import UIKit

public enum CAGradientPoint {
	case topLeft
	case centerLeft
	case bottomLeft
	case topCenter
	case center
	case bottomCenter
	case topRight
	case centerRight
	case bottomRight
	var point: CGPoint {
		switch self {
			case .topLeft:
				return CGPoint(x: 0, y: 0)
			case .centerLeft:
				return CGPoint(x: 0, y: 0.5)
			case .bottomLeft:
				return CGPoint(x: 0, y: 1.0)
			case .topCenter:
				return CGPoint(x: 0.5, y: 0)
			case .center:
				return CGPoint(x: 0.5, y: 0.5)
			case .bottomCenter:
				return CGPoint(x: 0.5, y: 1.0)
			case .topRight:
				return CGPoint(x: 1.0, y: 0.0)
			case .centerRight:
				return CGPoint(x: 1.0, y: 0.5)
			case .bottomRight:
				return CGPoint(x: 1.0, y: 1.0)
		}
	}
}

extension CAGradientLayer {
	
	convenience init(start: CAGradientPoint, end: CAGradientPoint, colors: [CGColor], type: CAGradientLayerType) {
		self.init()
		self.frame.origin = CGPoint.zero
		self.startPoint = start.point
		self.endPoint = end.point
		self.colors = colors
		self.locations = (0..<colors.count).map(NSNumber.init)
		self.type = type
	}
}

extension UIView {
	func addShadow() {
		self.layer.shadowColor = UIColor.gray.cgColor
		self.layer.shadowOffset = CGSize(width: 0, height: 2.0)
		self.layer.shadowRadius = 1.0
		self.layer.shadowOpacity = 0.5
		self.layer.masksToBounds = false
	}
}

extension UICollectionViewCell {
	/// Call this method from `prepareForReuse`, because the cell needs to be already rendered (and have a size) in order for this to work
	func shadowDecorate(radius: CGFloat = 32,
						shadowColor: UIColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3),
						shadowOffset: CGSize = CGSize(width: 0, height: 1.0),
						shadowRadius: CGFloat = 3,
						shadowOpacity: Float = 1) {
		contentView.layer.cornerRadius = radius
		contentView.layer.borderWidth = 1
		contentView.layer.borderColor = UIColor.clear.cgColor
		contentView.layer.masksToBounds = true
		
		layer.shadowColor = shadowColor.cgColor
		layer.shadowOffset = shadowOffset
		layer.shadowRadius = shadowRadius
		layer.shadowOpacity = shadowOpacity
		layer.masksToBounds = false
		layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: radius).cgPath
		layer.cornerRadius = radius
	}
}


extension UIColor {
		
	open class var pink: UIColor { return UIColor(red: 243 / 255.0, green: 115 / 255.0, blue: 189 / 255.0, alpha: 1) }
	open class var yellow: UIColor { return UIColor(red: 254 / 255.0, green: 220 / 255.0, blue: 157 / 255.0, alpha: 1) }
	open class var gray: UIColor { return UIColor(red: 240 / 255.0, green: 241 / 255.0, blue: 243 / 255.0, alpha: 1) }
	open class var red: UIColor { return UIColor(red: 250 / 255.0, green: 44 / 255.0, blue: 90 / 255.0, alpha: 1) }
	open class var blue: UIColor { return UIColor(red: 76 / 255.0, green: 104 / 255.0, blue: 239 / 255.0, alpha: 1) }
}


// MARK: Blur

protocol Blurable
{
	var layer: CALayer { get }
	var subviews: [UIView] { get }
	var frame: CGRect { get }
	var superview: UIView? { get }
	func removeFromSuperview()
	
	func blur(blurRadius: CGFloat)
}

extension Blurable
{
	func blur(blurRadius: CGFloat)
	{
		if self.superview == nil
		{
			return
		}
		
		UIGraphicsBeginImageContextWithOptions(CGSize(width: frame.width, height: frame.height), false, 1)
		
		layer.render(in: UIGraphicsGetCurrentContext()!)
		
		let image = UIGraphicsGetImageFromCurrentImageContext()
		
		UIGraphicsEndImageContext();
		
		guard let blur = CIFilter(name: "CIGaussianBlur"),
			  let this = self as? UIView else
		{
			return
		}
		
		blur.setValue(CIImage(image: image!), forKey: kCIInputImageKey)
		blur.setValue(blurRadius, forKey: kCIInputRadiusKey)
		
		let ciContext  = CIContext(options: nil)
		
		let result = blur.value(forKey: kCIOutputImageKey) as! CIImage?
		
		let boundingRect = CGRect(x:0,
								  y: 0,
								  width: frame.width,
								  height: frame.height)
		
		let cgImage = ciContext.createCGImage(result!, from: boundingRect)
		
		let filteredImage = UIImage(cgImage: cgImage!)
		
		let blurOverlay = BlurOverlay()
		blurOverlay.frame = boundingRect
		
		blurOverlay.image = filteredImage
		blurOverlay.contentMode = UIView.ContentMode.left
		
		if let superview = superview as? UIStackView,
		   let index = (superview as UIStackView).arrangedSubviews.firstIndex(of: this)
		{
			removeFromSuperview()
			superview.insertArrangedSubview(blurOverlay, at: index)
		}
		else
		{
			blurOverlay.frame.origin = frame.origin
			
			UIView.transition(from: this,
							  to: blurOverlay,
							  duration: 0.2,
							  options: .curveEaseIn,
							  completion: nil)
		}
		
		objc_setAssociatedObject(this,
								 &BlurableKey.blurable,
								 blurOverlay,
								 objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
	}
}

extension UIView: Blurable
{
	func applyGradient(startPoint:CGPoint, endPoint:CGPoint, radius: CGFloat, colours: [UIColor]) -> Void {
		let gradient: CAGradientLayer = CAGradientLayer()
		gradient.frame = self.bounds
		gradient.colors = colours.map { $0.cgColor }
		gradient.startPoint = startPoint
		gradient.endPoint = endPoint
		gradient.cornerRadius = radius
		self.layer.insertSublayer(gradient, at: 0)
	}
}
class BlurOverlay: UIImageView
{
}

struct BlurableKey
{
	static var blurable = "blurable"
}
