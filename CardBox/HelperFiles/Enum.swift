//
//  Enum.swift
//  CardBox
//
//  Created by Thomas Gallier on 12/04/2021.
//

import Foundation
import UIKit

enum Regex:String {
	case birthday = #"^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]|(?:Jan|Mar|May|Jul|Aug|Oct|Dec)))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2]|(?:Jan|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec))\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)(?:0?2|(?:Feb))\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9]|(?:Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep))|(?:1[0-2]|(?:Oct|Nov|Dec)))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$"#
	case name = #"^[\p{L}'-][\p{L}' -]{1,25}$"#
}

enum StoryboardName:String {
	case main = "Main"
	case boarding = "Boarding"
}

enum ControllerName:String {
	
	// Main
	case home = "HomeController"
	case detailCard = "DetailsCardController"
	
	// Settings
	case settings = "SettingsController"
	case accountInfo = "AccountInfoController"
	
	// Add Card
	case addCard = "AddCardController"
	case expirationCard = "ExpirationCardController"
	case cvvCard = "CVVCardController"
	
	// Sign Up
	case signUpName = "SignUpNameController"
	case signUpBirthday = "SignUpBirthdayController"
	case signUpMethod = "SignUpMethodController"
	case signUpEmail = "SignUpEmailController"
	
	// Sign In
	case signInMethod = "SignInMethodController"
	case signInEmail = "SignInEmailController"
		
	// Others
	case splashScreen = "SplashScreenController"
	case signMethod = "SignMethodController"
}

enum CellName:String {
	case cardView = "CardCell"
	case paymentCell = "PaymentCell"
}
