//
//  LoadingButton.swift
//  CardBox
//
//  Created by Thomas Gallier on 15/04/2021.
//

import UIKit

class LoadingButton: UIButton {
	
	var originalButtonText: String?
	var originalButtonIcon: UIImage?
	var activityIndicator: UIActivityIndicatorView!
	
	@IBInspectable
	let activityIndicatorColor: UIColor = .lightGray
	
	func showLoading() {
		originalButtonText = self.titleLabel?.text
		originalButtonIcon = self.currentImage
		self.setTitle("", for: .normal)
		self.setImage(nil, for: .normal)
		
		if (activityIndicator == nil) {
			activityIndicator = createActivityIndicator()
		}
		
		showSpinning()
	}
	
	func hideLoading() {
		self.setTitle(originalButtonText, for: .normal)
		self.setImage(self.originalButtonIcon, for: .normal)
		activityIndicator.stopAnimating()
	}
	
	private func createActivityIndicator() -> UIActivityIndicatorView {
		let activityIndicator = UIActivityIndicatorView()
		activityIndicator.hidesWhenStopped = true
		activityIndicator.color = activityIndicatorColor
		return activityIndicator
	}
	
	private func showSpinning() {
		activityIndicator.translatesAutoresizingMaskIntoConstraints = false
		self.addSubview(activityIndicator)
		centerActivityIndicatorInButton()
		activityIndicator.startAnimating()
	}
	
	private func centerActivityIndicatorInButton() {
		let xCenterConstraint = NSLayoutConstraint(item: self,
												   attribute: .centerX,
												   relatedBy: .equal,
												   toItem: activityIndicator,
												   attribute: .centerX,
												   multiplier: 1, constant: 0)
		self.addConstraint(xCenterConstraint)
		
		let yCenterConstraint = NSLayoutConstraint(item: self,
												   attribute: .centerY,
												   relatedBy: .equal,
												   toItem: activityIndicator,
												   attribute: .centerY,
												   multiplier: 1, constant: 0)
		self.addConstraint(yCenterConstraint)
	}
	
}
