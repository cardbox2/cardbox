//
//  Error.swift
//  CardBox
//
//  Created by Thomas Gallier on 14/04/2021.
//

import UIKit

enum EmbuscadeError: Error {
	case unknow
	
	func toString() -> String {
		switch self{
			case .unknow:
				return "Erreur inconnue"
		}
	}
}

extension UIViewController {
	func showAlert(title:String, description:String) {
		let alert = UIAlertController(title: title, message: description, preferredStyle: .alert)
		alert.addAction(UIAlertAction(title: "Fermer", style: .default, handler: nil))
		self.present(alert, animated: true, completion: nil)
	}
}
